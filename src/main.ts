import Keycloak, {KeycloakConfig} from "keycloak-js";

let initOptions: KeycloakConfig = {
  url: 'https://accounts.xlscout.ai/auth', realm: 'xlscout_dev', clientId: 'ideation-frontend'
}

const keycloak = Keycloak(initOptions);

(async () => {
  const auth = await keycloak.init({onLoad: 'login-required'})

  if (!auth) {
    window.location.reload();
  } else {
    console.log("Authenticated");

    // @ts-ignore
    const {default: initApp} = await import('./App.ts')

    initApp(keycloak)
  }
})()
