import instance from "@/api/instance";
import store from "@/store/store";
import {thirdStepItem} from "@/store/search";
import axios from "axios";

export const getSecondStepData = async (id: number): Promise<string[]> => {
  try {
    const {data: {data}} = await instance.post('/triz/features/', {feature_id: id}, {
      headers: {
        'Authorization': 'Bearer ' + store.state.user.token
      }
    })

    return Array.isArray(data) ? data : Object.values(data)
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error(e);
  }
  return []
}

export const getThirdStepData = async (): Promise<thirdStepItem[]> => {
  type cause = {
    mainCause: string,
    subCause1: string,
    subCause2?: string,
    subCause3?: string,
  }

  type details = {
    title: string,
    causeSelected: '1' | '2' | '3' | '4' | '5' | '6',
    cause1: cause,
    cause2: cause,
    cause3: cause,
    cause4: cause,

    cause5?: cause,
    cause6?: cause,
  }

  const {selectedCause, causesData, problemTitle} = store.state.search

  const case_details: details = ((): details => {
    const next = {
      title: problemTitle,
      causeSelected: `${selectedCause + 1}`
    }
    // @ts-ignore
    causesData.forEach(({cause, subCauses}, index) => {
      const nextCause = {
        mainCause: cause
      }
      // @ts-ignore
      subCauses.forEach((c: any, subIndex: any) => nextCause[`subCause${subIndex + 1}`] = c)
      // @ts-ignore
      next[`cause${index + 1}`] = nextCause
    })
    // @ts-ignore
    return next
  })()


  try {
    const params = new URLSearchParams()
    params.append('case_details', JSON.stringify(case_details))

    const config = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + store.state.user.token,
      }
    }

    const {data: {data}} = await instance.post('/novelty/features/', params, config)

    //@ts-ignore
    return data.kfs.split(/;|XlpatKeyFertures/).map((text, index) => ({text, selected: false, id: Date.now() + index}))
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error(e);
  }

  return []
}

export const getTechnologies = async (): Promise<void> => {
  try {
    const config = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + store.state.user.token,
      }
    }
    const {data} = await axios.get('https://api-idea-dev.xlscout.ai/technology/', config)
    store.commit('search/setTechnologies', data.map((item: {}) => ({...item, selected: false})))
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error(e);
  }
}

export const getTechnologiesFields = async (): Promise<void> => {
  try {
    const log_id = '6038a7e30a93713cbe62bc7a'
    const divider = 'XlpatKeyFertures'
    const title = store.state.search.problemTitle
    // @ts-ignore
    const kfv = (store.state.search.thirdStepItems as thirdStepItem[]).reduce((accum, curr) => {
      return accum ? `${accum} ${divider} ${curr.text.trim()}` : curr.text.trim()
    }, '')
    const kfk = (store.state.search.thirdStepItems as thirdStepItem[]).map(({selected}) => selected)

    const bodyFormData = new FormData();

    bodyFormData.append('log_id', log_id);
    bodyFormData.append('title', title);
    bodyFormData.append('kfv', kfv);
    bodyFormData.append('kfk', JSON.stringify(kfk));

    const {data:{data:  [info]}} = await axios({
      method: "post",
      url: "https://api-idea-dev.xlscout.ai/novelty/cpc/title/",
      data: bodyFormData,
      headers: { "Content-Type": "multipart/form-data", 'Authorization': 'Bearer ' + store.state.user.token },
    })


    store.commit('search/setTechnologiesFields', info.map(({class: classCode, text}:{class: string, text: string}) => ({classCode, text, selected: false})))

  } catch (e) {
    // eslint-disable-next-line no-console
    console.error(e);
  }
}