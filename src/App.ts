import Vue from 'vue'
import App from './App.vue'
import store from "@/store/store";
import router from "@/routes";
import i18n from "@/lang/lang";
import BootstrapVue, {IconsPlugin} from "bootstrap-vue";
// @ts-ignore
import VueGoodTablePlugin from 'vue-good-table'
import {KeycloakInstance} from "keycloak-js";
import 'vue-good-table/dist/vue-good-table.css'

import "@/components/common"

const initApp = async (keycloak: KeycloakInstance) => {
  Vue.use(BootstrapVue);
  Vue.use(IconsPlugin);
  Vue.use(VueGoodTablePlugin);

  // @ts-ignore
  const {name, preferred_username: preferredUsername, email} = await keycloak.loadUserInfo()
  await store.dispatch('user/login', {
    // @ts-ignore
    userName: name ?? preferredUsername,
    token: keycloak.token
  })

  Vue.prototype.$keycloak = keycloak

  new Vue({
    store,
    router,
    i18n,
    render: h => h(App),
  }).$mount('#app')

  setInterval(() => {
    keycloak.updateToken(70).then((refreshed) => {
      if (refreshed) {
        console.info('Token refreshed' + refreshed);
        store.commit('user/setToken', keycloak.token)
      } else {
        if (keycloak.tokenParsed?.exp && keycloak.timeSkew) {
          console.warn('Token not refreshed, valid for '
            + Math.round(keycloak.tokenParsed.exp + keycloak.timeSkew - new Date().getTime() / 1000) + ' seconds');
        }
      }
    }).catch(() => {
      console.error('Failed to refresh token');
    });
  }, 6000)
}

export default initApp