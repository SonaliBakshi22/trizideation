import Vue from "vue";
import VueRouter, {RouterOptions} from 'vue-router'

const Search = () => import("@/components/containers/searchFunnel/searchFunnel.vue");
const History = () => import("@/components/containers/history/history.vue");
const Help = () => import("@/components/containers/help/help.vue");

Vue.use(VueRouter);

export const routerOptions: RouterOptions = {
  routes: [
    {
      path: '/search',
      component: Search,
    },
    {
      path: '/history',
      component: History,
    },
    {
      path: '/help',
      component: Help,
    },
    {
      path: '*',
      redirect: '/search'
    }
  ],
  mode: 'history',
}

export default new VueRouter(routerOptions)