import {GetterTree, MutationTree, ActionTree, ActionContext} from "vuex"
import {getSecondStepData, getThirdStepData} from "@/api/search";

export type stepType = 'first' | 'second' | 'third' | 'fourth' | 'fifth' | 'outputStep'

export enum stepsIdentity {
  'first' = 1, 'second', 'third', 'fourth', 'fifth', 'outputStep'
}

export enum stepsTitle {
  'Problem@Disclosure' = 1,
  'Selecting Technical@Contradiction Parameters',
  'Supervise the@Machine',
  'Technology Area@Selection',
  'Technical Variations Of@Key Concepts',
  'Selecting Type of@Output'
}

export enum parametersToImprove {
  "Weight of Moving Object" = 1,
  "Weight of Stationary Object",
  "Length of Moving Object",
  "Length of Stationary Object",
  "Area of Moving Object",
  "Area of Stationary Object",
  "Volume of Moving Object",
  "Volume of Stationary Object",
  "Speed",
  "Force",
  "Stress or Pressure",
  "Shape",
  "Stability of the Object's Composition",
  "Strength",
  "Duration of Action by a Moving Object",
  "Duration of Action by a Stationary Object",
  "Temperature",
  "Illumination Intensity",
  "Use of Energy by Moving Object",
  "Use of Energy by Stationary Object",
  "Power",
  "Loss of Energy",
  "Loss of Substance",
  "Loss of Information",
  "Loss of Time",
  "Quantity of Substance",
  "Reliability",
  "Measurement Accuracy",
  "Manufacturing Precision",
  "External Harm Affects the Object",
  "Object-Generated Harmful Factors",
  "Ease of Manufacture",
  "Ease of Operation",
  "Ease of Repair",
  "Adaptability or Versatility",
  "Device Complexity",
  "Difficulty of Detecting and Measuring",
  "Extent of Automation",
  "Productivity",
}

export type thirdStepItem = { text: string, selected: boolean, id: number }

export type cause = { cause: string, subCauses: string[] }

export type technology = {
  id: number,
  name: string,
  selected: boolean
}

export type technologyField = {
  classCode: string,
  text: string,
  selected: boolean
}

interface State {
  step: stepType,

  selectedCause: number,
  problemTitle: string,
  causesData: cause[],
  causesDataChanged: boolean,
  fetchedParametersToPreserve: { [key: string]: string[] }
  parameterToImprove: number
  parameterToPreserve: string,
  thirdStepItems: thirdStepItem[]
  technologies: technology[]
  technologyFields: technologyField[]
}

const storedState: any = JSON.parse(localStorage.getItem('searchSettings') ?? '{}')
const state: State = {
  step: 'first',

  selectedCause: -1,
  problemTitle: '',
  causesData: [
    {cause: '', subCauses: ['']},
    {cause: '', subCauses: ['']},
    {cause: '', subCauses: ['']},
    {cause: '', subCauses: ['']},
  ],
  causesDataChanged: false,

  parameterToImprove: -1,
  fetchedParametersToPreserve: {},
  parameterToPreserve: '',

  thirdStepItems: [],

  technologies: [],
  technologyFields: [],

  ...storedState
}

const mutations = <MutationTree<State>>{
  setStep(state, payload: stepType) {
    state.step = payload
  },

  setSelectedCause(state, payload: number) {
    state.causesDataChanged = true
    state.selectedCause = payload
  },

  setProblemTitle(state, payload: string) {
    state.causesDataChanged = true
    state.problemTitle = payload
  },

  setCausesData(state, payload: cause[]) {
    state.causesDataChanged = true
    state.causesData = payload
  },

  setCausesDataChanged(state, payload: boolean) {
    state.causesDataChanged = payload
  },

  setTechnologies(state, payload: technology[]) {
    state.technologies = payload
  },

  setTechnologiesFields(state, payload: technologyField[]) {
    state.technologyFields = payload
  },

  setParameterToImprove(state, payload: number) {
    state.parameterToImprove = payload
  },
  setParameterToPreserve(state, payload: string) {
    state.parameterToPreserve = payload
  },
  setFetchedParametersToPreserve(state, payload: {}) {
    state.fetchedParametersToPreserve = payload
  },

  setThirdStepItems(state, payload: thirdStepItem[]) {
    state.thirdStepItems = payload
  }
};

const actions = <ActionTree<State, any>>{
  changeStep({commit, dispatch}, payload: number | stepType) {
    if (stepsIdentity[payload]) {
      commit('setStep', typeof payload === 'number' ? stepsIdentity[payload] : payload)
    }

    dispatch('saveStore')
  },

  async setParameterToImprove({commit, state, dispatch}, {
    id,
    shouldClearPreserve = true
  }: { id: number, shouldClearPreserve: boolean }) {
    if (!state.fetchedParametersToPreserve[parametersToImprove[id]]) {
      const data = await getSecondStepData(id)
      commit('setFetchedParametersToPreserve', {...state.fetchedParametersToPreserve, [parametersToImprove[id]]: data})
    }
    if (shouldClearPreserve) {
      commit('setParameterToPreserve', '')
    }
    commit('setParameterToImprove', id)

    dispatch('saveStore')
  },

  async getThirdStepData({commit, state}) {
    const data = await getThirdStepData()

    commit('setThirdStepItems', data)
    commit('setCausesDataChanged', false)
  },

  saveCausesData({commit, state}, v: cause[]) {
    if (state.selectedCause === -1) {
      const index = v.findIndex(({cause, subCauses}) => cause && subCauses.some(c => c))
      commit('setSelectedCause', index)
    } else {
      const selectedCause = v[state.selectedCause]
      if (!selectedCause.cause || !selectedCause.subCauses.some(c => c)) {
        const index = v.findIndex(({cause, subCauses}) => cause && subCauses.some(c => c))
        commit('setSelectedCause', index)
      }
    }

    commit('setCausesData', v)
  },

  async getTechnologies({commit, state}) {
    const data = await getThirdStepData()

    commit('setThirdStepItems', data)
    commit('setCausesDataChanged', false)
  },

  saveStore({state}) {
    localStorage.setItem('searchSettings',
      JSON.stringify({
        ...state,
        fetchedParametersToPreserve: {}
      }))
  }
};

const getters = <GetterTree<State, any>>{}

const search = {
  namespaced: true,
  state,
  mutations: mutations,
  actions: actions,
  getters,
  modules: {}
};

export default search;