import {GetterTree, MutationTree, ActionTree} from "vuex"

interface State {

}

const state: State = {

}

const mutations = <MutationTree<State>>{
  // setConcentrateOnResult(state, payload: boolean) {
  //   state.concentrateOnResult = payload
  // },
};

const actions = <ActionTree<State, any>>{

  // clearHighlighter({commit, getters, state}) {
  //   commit('setQueryHighlighterSettings', {
  //     ...state.highlighter.queryHighlighter,
  //     keywords: state.highlighter.queryHighlighter.keywords.filter(h => getters.queryHighlighterKeywords.includes(h))
  //   })
  // },

};

const getters = <GetterTree<State, any>>{

  // pagesCount(state, getters) {
  //   return Math.ceil(getters.counts.main / state.patentsPerPage)
  // },

}

const result = {
  namespaced: true,
  state,
  mutations: mutations,
  actions: actions,
  getters,
};

export default result;