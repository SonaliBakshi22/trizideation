import Vue from 'vue'
import Vuex from 'vuex'
import search from "@/store/search";
import user from "@/store/user";

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    search,
    user
  }
})


store.subscribe(((mutation) => {
  const [storeName, mut] = mutation.type.split(/\//)

  if (storeName === 'search') {
    store.dispatch('search/saveStore')
  }
}))


export default store