import {ActionTree, GetterTree, MutationTree} from "vuex";



interface State {

}

const state: State = {

}

export const mutations = <MutationTree<State>>{
  // setLastItems(state, payload) {
  //   state.lastItems = payload
  // },
};

export const actions = <ActionTree<State, any>>{
  // async fetchFirstItems({commit}) {
  //   const {items: saved, total} = await getHistory()
  //
  //   commit('setPage', 1)
  // },
};

const getters = <GetterTree<State, any>>{
  // pagesCount(state) {
  //   return Math.ceil(state.itemsCount / state.itemsPerPage)
  // },
}

const history = {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};

export default history;