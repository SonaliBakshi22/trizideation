import {ActionTree, GetterTree, MutationTree} from "vuex";

const state = {
  userName: '',
  token: '',
}

type State = typeof state

const getters = <GetterTree<State, any>>{
};

export const mutations = <MutationTree<State>>{
  setUserName(state: State, payload) {
    state.userName = payload
  },

  setToken(state: State, payload: string) {
    state.token = payload
  }
};

export const actions = <ActionTree<State, any>>{
  login({commit, state}, data) {
    commit('setUserName', data.userName)
    commit('setToken', data.token)
  }
};

const user = {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};

export default user;